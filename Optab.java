import java.util.*; 


public class Optab{
	public static Hashtable<String, String> format1 = new Hashtable<String, String>();
	public static Hashtable<String, String> format2 = new Hashtable<String, String>();
	public static Hashtable<String, String> format3 = new Hashtable<String, String>();
	public static Hashtable<String, String> format4 = new Hashtable<String, String>();

	public void createHashTable() {
		
		format1.put("FIX", "C4");   		
		format1.put("FLOAT", "C0");
		format1.put("HIO", "F4");
		format1.put("NORM", "C8");
		format1.put("SIO", "F0");
		format1.put("TIO", "F8");

		format2.put("ADDR", "90");
		format2.put("CLEAR", "B4");
		format2.put("COMPR", "A0");
		format2.put("DIVR", "64");
		format2.put("MULR", "98");
		format2.put("RMO", "AC");
		format2.put("SHIFTL", "A4");
		format2.put("SHIFTR", "A8");
		format2.put("SUBR", "94");
		format2.put("SVC", "B0");
		format2.put("TIXR", "B8");

		format3.put("ADD", "18");
		format3.put("ADDF", "58");
		format3.put("AND", "40");
		format3.put("COMP", "28");
		format3.put("COMPF", "88");
		format3.put("DIV", "24");
		format3.put("DIVF", "64");
		format3.put("J", "3C");
		format3.put("JEQ", "30");
		format3.put("JGT", "34");
		format3.put("JLT", "38");
		format3.put("JSUB", "48");
		format3.put("LDA", "00");
		format3.put("LDB", "68");
		format3.put("LDCH", "50");
		format3.put("LDF", "70");
		format3.put("LDL", "08");
		format3.put("LDS", "6C");
		format3.put("LDT", "74");
		format3.put("LDX", "04");
		format3.put("LPS", "D0");
		format3.put("MULF", "60");
		format3.put("OR", "44");
		format3.put("RD", "D8");
		format3.put("RSUB", "4C");
		format3.put("SSK", "EC");
		format3.put("STA", "0C");
		format3.put("STB", "78");
		format3.put("STCH", "54");
		format3.put("STF", "80");
		format3.put("STI", "D4");
		format3.put("STL", "14");
		format3.put("STS", "7C");
		format3.put("STSW", "E8");
		format3.put("STT", "84");
		format3.put("STX", "10");
		format3.put("SUB", "1C");
		format3.put("SUBF", "5C");
		format3.put("TD", "E0");
		format3.put("TIX", "2C");
		format3.put("WD", "DC");

		format4.put("+ADD", "18");
		format4.put("+ADDF", "58");
		format4.put("+AND", "40");
		format4.put("+COMP", "28");
		format4.put("+COMPF", "88");
		format4.put("+DIV", "24");
		format4.put("+DIVF", "64");
		format4.put("+J", "3C");
		format4.put("+JEQ", "30");
		format4.put("+JGT", "34");
		format4.put("+JLT", "38");
		format4.put("+JSUB", "48");
		format4.put("+LDA", "00");
		format4.put("+LDB", "68");
		format4.put("+LDCH", "50");
		format4.put("+LDF", "70");
		format4.put("+LDL", "08");
		format4.put("+LDS", "6C");
		format4.put("+LDT", "74");
		format4.put("+LDX", "04");
		format4.put("+LPS", "D0");
		format4.put("+MULF", "60");
		format4.put("+OR", "44");
		format4.put("+RD", "D8");
		format4.put("+RSUB", "4C");
		format4.put("+SSK", "EC");
		format4.put("+STA", "0C");
		format4.put("+STB", "78");
		format4.put("+STCH", "54");
		format4.put("+STF", "80");
		format4.put("+STI", "D4");
		format4.put("+STL", "14");
		format4.put("+STS", "7C");
		format4.put("+STSW", "E8");
		format4.put("+STT", "84");
		format4.put("+STX", "10");
		format4.put("+SUB", "1C");
		format4.put("+SUBF", "5C");
		format4.put("+TD", "E0");
		format4.put("+TIX", "2C");
		format4.put("+WD", "DC");
	}


	public void findLengths() {
		int counter = 0;
		int counter2 = 0;
		for (int i = 1; i < Main.instructions.size(); i++) {
			if (format1.containsKey(Main.instructions.get(i))){
				Main.instrlengths.add("1");
			}
			if (format2.containsKey(Main.instructions.get(i))){
				Main.instrlengths.add("2");
			}
			if (format3.containsKey(Main.instructions.get(i))) {
				Main.instrlengths.add("3");
			}
			if (format4.containsKey(Main.instructions.get(i))) {
				Main.instrlengths.add("4");
			} 
			if (Main.instructions.get(i).equals("RESW")) {
				int index = Main.indexes.get("RESW").get(counter);
				int num = Integer.parseInt(Main.inputs.get(index+1));
				num = num * 3;
				String number = Integer.toHexString(num);
				counter++;
				Main.instrlengths.add(number);
				Main.minus++;


			}
			if (Main.instructions.get(i).equals("WORD")) {
				Main.instrlengths.add("3");
				Main.minus++;
				
			}
			if (Main.instructions.get(i).equals("BYTE")) {
				Main.instrlengths.add("1");
				Main.minus++;
			}
			if (Main.instructions.get(i).equals("RESB")) {
				int index = Main.indexes.get("RESB").get(counter2);
				int num = Integer.parseInt(Main.inputs.get(index+1));
				String number = Integer.toString(num);
				counter2++;
				Main.instrlengths.add(number);
				Main.minus++;
			}

			if (Main.instructions.get(i).equals("BASE")) {
				Main.instrlengths.add("0");
				Main.minus++;
			}

			
			
		}

		Main.addresses.add(Main.startingAddress);
		Main.addresses.add(Main.startingAddress);
		

		//Converts addresses into hex and adds the lengths of the instructions
		for (int i = 0; i < Main.instrlengths.size(); i++) {
			int decimal=Integer.parseInt(Main.startingAddress,16);
			int decimal1 = Integer.parseInt(Main.instrlengths.get(i),16);
			Main.startingAddress = (Integer.toHexString(decimal1 + decimal));
			Main.addresses.add(Main.startingAddress);
		}
		System.out.println("Addresses(location):" + "\n" + Main.addresses + "\n");
	}
	
}