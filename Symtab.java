import java.util.*; 

public class Symtab {
    public void createSymtab() {
        int k = 0;
        System.out.println("");
        System.out.println("~SYMTAB~");
        for (int i = 0; i < Main.linelengths.size(); i++) {
            if (Main.linelengths.get(i).equals(3)) {
                System.out.println(Main.labels.get(k) + ": " + Main.addresses.get(i));
                Main.Symtab.put(Main.labels.get(k), Main.addresses.get(i));
                k++;
            }   
        }
    }
}
