import java.io.*; 
import java.util.ArrayList;
import java.util.Scanner;
import java.io.File;
import java.util.Arrays;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.io.LineNumberReader;

//import java.math.BigInteger;
public class Main
{ 

  //intializing the numbers of lines and starting address.
  public static int linenumber;
  public static String FINALSTARTINGADDRESS;
  public static String PROGRAMNAME;
  public static String startingAddress;
  //These are ArrayList that hold labels, instructions (mneumonics), Operand.
  public static ArrayList<String> labels = new ArrayList<String>(); 
  public static ArrayList<String> instructions = new ArrayList<String>(); 
  public static ArrayList<String> operand = new ArrayList<String>(); 
  //ArrayList that contains all the individual words from the text file
  public static ArrayList<String> inputs = new ArrayList<String>(); 
  //The length of the instruction to be added to the last starting address
  public static ArrayList<String> instrlengths = new ArrayList<String>();
  //The number of words in a line
  public static ArrayList<Integer> linelengths = new ArrayList<Integer>();
  //The addresses 
  public static ArrayList<String> addresses = new ArrayList<String>();
  //A list of all the opcodes
  public static ArrayList<String> opcodelist = new ArrayList<String>();
  public static String flagbits;
  public static String bitn;
  public static String biti;
  public static String bitx;
  public static String bitb;
  public static String bitp;
  public static String bite;
  public static String displacment20;
  public static String displacement20;
  public static Integer minus = 2; 

  public static HashMap<String, ArrayList<Integer>> indexes = new HashMap<>();
  public static ArrayList<String> flagbitslist = new ArrayList<String>();
  public static ArrayList<String> flagbitslabels = new ArrayList<String>();
  public static ArrayList<Integer> tracker = new ArrayList<Integer>();
  public static ArrayList<String> tracker2 = new ArrayList<String>();
  public static int XREGISTER = 0;
  public static int BREGISTER;
  public static String AREGISTER = "0";
  public static String LREGISTER = "2";
  public static String SREGISTER = "4";
  public static String TREGISTER = "5";
  public static String FREGISTER = "6";
  public static String SWREGISTER = "8";

  


  public static ArrayList<String> FINALADDRESS = new ArrayList<String>();
  public static ArrayList<String> TEXTRECORDS = new ArrayList<String>();
  public static ArrayList<Integer> LENGTHRECORDS = new ArrayList<Integer>();
  public static ArrayList<String> FINALOPLIST = new ArrayList<String>();
  public static ArrayList<String> STARTADDR = new ArrayList<String>();
  public static ArrayList<Integer> MODIFICATIONS = new ArrayList<Integer>();

  public static HashMap<String, String> Symtab = new HashMap<>();

  
  public static void main(String[] args)throws Exception 
  { 
    
    

    String dir = System.getProperty("user.dir");
    File file = new File(dir  + "/Test/test.text");
    FileReader fr = new FileReader(file);
    BufferedReader br = new BufferedReader(fr);
    String s;
    Scanner input = new Scanner(file); 


    int count = 0;
    linenumber = 0;

    while (input.hasNext()) {
      String word  = input.next();
      inputs.add(word);
      count = count + 1;
    } 
    int track = 0;
    while ((s=br.readLine()) != null){
      String[] tokens = s.trim().split("\\s+");
      linelengths.add(tokens.length);
      if (tokens.length == 2) {
        instructions.add(tokens[0]);
        operand.add(tokens[1]);
      } else if(tokens.length == 3) {
          labels.add(tokens[0]);
          instructions.add(tokens[1]);
          operand.add(tokens[2]);
      } else {
        instructions.add(tokens[0]);
      }
      track++;
      linenumber++;
    }

    
    for (int i = 0; i < inputs.size(); i++) {
        indexes.computeIfAbsent(inputs.get(i), c -> new ArrayList<>()).add(i);
    }
    System.out.println(indexes);

    
    System.out.println("Word count: " + count);
    System.out.println("Lines: " + linenumber);
    
    PROGRAMNAME = inputs.get(0);


    if (inputs.get(0).equals("START")) {
      startingAddress = inputs.get(1);

      }
    if (inputs.get(1).equals("START")) {
      startingAddress = inputs.get(2);
      }
    FINALSTARTINGADDRESS = startingAddress;

    System.out.println("Starting Address: " + startingAddress + "\n");
    System.out.println("All Words in TextFile:" +"\n"+ inputs + "\n");
    System.out.println("Labels:" + "\n" + labels + "\n");
    System.out.println("Instructions:" + "\n" + instructions + "\n");
    System.out.println("Operand:" + "\n" + operand + "\n");
    System.out.println("Number of words in a line in TEXTFILE:" + "\n" + linelengths + "\n");
 

 

    Optab optable = new Optab();
    optable.createHashTable();
    optable.findLengths();
    Symtab symtable = new Symtab();
    symtable.createSymtab();


    for (int i = 1; i < inputs.size(); i++) {
      if (optable.format1.containsKey(inputs.get(i))) {
        opcodelist.add(optable.format1.get(inputs.get(i)));
        tracker.add(i);
      }
      if (optable.format2.containsKey(inputs.get(i))) {
        opcodelist.add(optable.format2.get(inputs.get(i)));
        tracker.add(i);
      }
      if (optable.format3.containsKey(inputs.get(i))) {
        String val = optable.format3.get(inputs.get(i));
        int m = Integer.parseInt(val, 16);
        String bin = Integer.toBinaryString((m & 0xFF) + 0x100).substring(1);
        opcodelist.add(bin);
        tracker.add(i);
      }
      if (optable.format4.containsKey(inputs.get(i))) {
        String val = optable.format4.get(inputs.get(i));
        int m = Integer.parseInt(val, 16);
        String bin = Integer.toBinaryString((m & 0xFF) + 0x100).substring(1);
        opcodelist.add(bin);
        tracker.add(i);
        MODIFICATIONS.add(i);
      }

      if (inputs.get(i).equals("BASE")) {
        BREGISTER = Integer.parseInt(Symtab.get(inputs.get(i+1)),16);
      }

    }

    int place = 0;
    for (int i = 0; i < tracker.size(); i++) {
      int index = tracker.get(i);
      String key = inputs.get(index);
      String value = inputs.get(index+1);
      if (key.equals("RSUB")) {
        bitn = "1";
        biti = "1";
        bitx = "0";
        bitb = "0";
        bitp = "0";
        bite = "0";
        value = "NULL";
      } else if (key.charAt(0) == '+') {
          if (value.charAt(0) == '#') {
            bitn = "0";
            biti = "1";
            bitx = "0";
            bitb = "0";
            bitp = "0";
            bite = "100000000000000000000";
            value = value.substring(1,value.length());
          } else if (value.charAt(0) == '@') {
            bitn = "1";
            biti = "0";
            bitx = "0";
            bitb = "0";
            bitp = "0";
            bite = "100000000000000000000";
            value = value.substring(1,value.length());
          } else {
            bitn = "1";
            biti = "1";
            bitx = "0";
            bitb = "0";
            bitp = "0";
            bite = "100000000000000000000";
            
          }
        } else {
          bite = "0";
          if (value.charAt(0) == '#') {
            value = value.substring(1,value.length());
            bitn = "0";
            biti = "1";
            if (isNumeric(value)) {
            bitp = "0";
            bitb = "0";
          } else {
            bitp = "1";
            bitb = "0";
            }
          } else if (value.charAt(0) == '@') {
              value = value.substring(1,value.length());
              bitn = "1";
              biti = "0";
              if (isNumeric(value)) {
                bitp = "0";
                bitb = "0";
              } else {
                bitp = "1";
                bitb = "0";
                }
              } else {
                  bitn = "1";
                  biti = "1";
                  if (optable.format2.containsKey(key)) {
                    bitn = "";
                    biti = "";
                    bitx = "";
                    bitb = "";
                    bitp = "";
                    bite = "";
                        
                    }
                  if (isNumeric(value)) {
                    bitp = "0";
                    bitb = "0";
                  } else {
                    bitp = "1";
                    bitb = "0";
                    }

                  }
          
          
          char last = value.charAt(value.length() - 1);
            if (last == 'X') {
              bitx = "1";
              value = value.substring(0,value.length()-2);
              if (isNumeric(value)) {
                bitp = "0";
              }
            } else {
              bitx = "0";
            }
        }
          char last = value.charAt(value.length() - 1);
            if (last == 'X') {
              bitx = "1";
              value = value.substring(0,value.length()-2);
              if (isNumeric(value)) {
                bitp = "0";
              }
            } else {
              bitx = "0";
            }
        place++;
        flagbitslist.add(bitn + biti + bitx + bitb + bitp + bite);
        flagbitslabels.add(key);
        tracker2.add(value);


        

        }

      
    
    System.out.println(tracker2);
    System.out.println("Executable Instructions: " + "\n" + flagbitslabels + "\n");
    System.out.println(flagbitslist);


    for (int i = 0; i < opcodelist.size(); i++) {
      if (optable.format2.containsKey(flagbitslabels.get(i))) {
        String form2op = opcodelist.get(i);
        String sec = tracker2.get(i);
        if (tracker2.get(i).length() < 3) {
          sec = sec + ",X";
        }
        String r1 = sec.substring(0,1);
        String r2 = sec.substring(2,3);
        System.out.println(tracker2.get(i));

        if (r1.equals("T")) {
          r1 = "5";
        }
        if (r1.equals("S")) {
          r1 = "4";
        }
        if (r1.equals("X")) {
          r1 = "1";
        }
        if (r1.equals("A")) {
          r1 = "0";
        }
        if (r1.equals("L")) {
          r1 = "2";
        }
        if (r1.equals("B")) {
          r1 = "3";
        }

        if (r2.equals("T")) {
          r2 = "5";
        }
        if (r2.equals("S")) {
          r2 = "4";
        }
        if (r2.equals("X")) {
          r2 = "1";
        }
        if (r2.equals("A")) {
          r2 = "0";
        }
        if (r2.equals("L")) {
          r2 = "2";
        }
        if (r2.equals("B")) {
          r2 = "3";
        }
        form2op = form2op + r1 + r2;
        FINALOPLIST.add(form2op);


      }

      //stores the x register when LDX
      if (flagbitslabels.get(i).equals("LDX") || flagbitslabels.get(i).equals("+LDX")) {
        String x = tracker2.get(i);
        int val1 = Integer.parseInt(x,16);
        XREGISTER = val1;
      }
      String val;
      String hexString;
      //checks if its format4
      if (flagbitslist.get(i).length() == 26) {
        if (flagbitslist.get(i).equals("111001")) {
          String addr = Symtab.get(tracker2.get(i));
          int addrint = Integer.parseInt(addr,16);
          addrint++;
          val = opcodelist.get(i).substring(0,6);
          val = val + flagbitslist.get(i).substring(0,6) + "0000";
          hexString = Long.toHexString(Long.parseLong(val, 2));
          hexString = hexString + Integer.toHexString(addrint);
          int num = 8 - hexString.length();
          if (num > 0) {
            hexString = "0" + hexString;
          }
          FINALOPLIST.add(hexString);

        } else {
          val = opcodelist.get(i).substring(0,6);
          val = val + flagbitslist.get(i).substring(0,6) + "0000";
          hexString = Long.toHexString(Long.parseLong(val, 2));
          hexString = hexString + Symtab.get(tracker2.get(i));
          int num = 8 - hexString.length();
          if (num > 0) {
            hexString = "0" + hexString;
          }
          FINALOPLIST.add(hexString);
        }
      } else {
          //checks if they are PC + DISP type addressing modes
          if (flagbitslist.get(i).equals("110010") || flagbitslist.get(i).equals("100010") 
                  || flagbitslist.get(i).equals("010010") || flagbitslist.get(i).equals("111010")) {
            
            String TA = Symtab.get(tracker2.get(i));
            String PC = addresses.get(lineFinder(tracker.get(i))+1);
            int TAint = Integer.parseInt(TA,16);
            int PCint = Integer.parseInt(PC,16);
            int disp = TAint - PCint;
          
            if (flagbitslist.get(i).equals("111010") || flagbitslist.get(i).equals("111010")) {
              disp = disp - XREGISTER;
            }

          
            if (disp > 2047 ||disp < -2048) {
              String changeFlag = flagbitslist.get(i);
              changeFlag = changeFlag.substring(0,3) + "10" + changeFlag.substring(5);
              if (changeFlag.equals("111100")) {
                disp = TAint - BREGISTER - XREGISTER;
              } else {
                disp = TAint - BREGISTER;
              }

              StringBuilder sb = new StringBuilder();
              sb.append(Integer.toHexString(disp));
              String hex = sb.toString();
              int space = 3 - hex.length() ;
              if (space > 0) {
                for (int j = 0; j < space; j++) {
                  hex = hex + "0";
                }
              }
               val = opcodelist.get(i).substring(0,6);
               val = val + changeFlag;
               val = Long.toHexString(Long.parseLong(val, 2));
               val = val + hex;

              FINALOPLIST.add(val);

            } else {
              String dispHEX = Integer.toHexString(disp);
              int sub = dispHEX.length() - 3;
              if (sub > 0) {
                val = opcodelist.get(i).substring(0,6);
                dispHEX = dispHEX.substring(sub,dispHEX.length());
                val = val + flagbitslist.get(i);
                val = Long.toHexString(Long.parseLong(val, 2));
                val = val + dispHEX;
                FINALOPLIST.add(val);
              } else if (dispHEX.length() == 2) {
                dispHEX = "0"+dispHEX;
                val = opcodelist.get(i).substring(0,6);
                val = val + flagbitslist.get(i);
                val = Long.toHexString(Long.parseLong(val, 2));
                val = val + dispHEX;
                FINALOPLIST.add(val);
              } else if (dispHEX.length() == 1) {
                dispHEX = "00" + dispHEX;
                val = opcodelist.get(i).substring(0,6);
                val = val + flagbitslist.get(i);
                val = Long.toHexString(Long.parseLong(val, 2));
                val = val + dispHEX;
                FINALOPLIST.add(val);
              } else {
                val = opcodelist.get(i).substring(0,6);
                val = val + flagbitslist.get(i);
                val = Long.toHexString(Long.parseLong(val, 2));
                val = val + dispHEX;
                FINALOPLIST.add(val);

              }
            }

          
          

          
            
          }
          if (flagbitslist.get(i).equals("111000")) {
            String TA = tracker2.get(i);
            int TAint = Integer.parseInt(TA,16);
            int disp = TAint + XREGISTER;
            val = opcodelist.get(i).substring(0,6);
            val = val + flagbitslist.get(i).substring(0,6) + "0000";
            hexString = Long.toHexString(Long.parseLong(val, 2));
            if (hexString.length() == 1) {
              hexString = hexString + "000";
            }
            if (hexString.length() == 2) {
              hexString = hexString + "00";
            }
            if (hexString.length() == 3) {
              hexString = hexString + "0";
            }
            hexString = hexString + Integer.toHexString(disp);

            if (hexString.length() < 6) {
              hexString = "0" + hexString;
            }
            FINALOPLIST.add(hexString);

          }



          if (flagbitslist.get(i).equals("110000") || flagbitslist.get(i).equals("100000") || flagbitslist.get(i).equals("010000")) {
            if (opcodelist.get(i).equals("01001100")) {
              FINALOPLIST.add("4f0000");
            } else {
              String TA = tracker2.get(i);
              int TAint = Integer.parseInt(TA,16);
              int disp = TAint + XREGISTER;
              val = opcodelist.get(i).substring(0,6);
              val = val + flagbitslist.get(i).substring(0,6) + "0000";
              hexString = Long.toHexString(Long.parseLong(val, 2));
              if (hexString.length() == 1) {
              hexString = hexString + "000";
              }
              if (hexString.length() == 2) {
                hexString = hexString + "00";
              }
              if (hexString.length() == 3) {
                hexString = hexString + "0";
              }
              hexString = hexString + Integer.toHexString(disp);

              if (hexString.length() < 6) {
                hexString = "0" + hexString;
              }
              FINALOPLIST.add(hexString);
            }



            }

      }

 


    }

    System.out.println(FINALOPLIST);
    
    for (int i = 0; i < tracker.size(); i++) {
      int line = lineFinder(tracker.get(i));
      FINALADDRESS.add(addresses.get(line));
    }

  


    //MAKING OBJECT CODE

    //CONVERTING String to Decimal then back to Hex. 
    String hexNumber = addresses.get(addresses.size()-1);
    String hexNumber2 = FINALADDRESS.get(0);
    int decimal = Integer.parseInt(hexNumber, 16);  
    int decimal2 = Integer.parseInt(hexNumber2, 16);  
    int programLength = decimal - decimal2;

    String FinalProglength = Integer.toHexString(programLength);
    int sub2 = 6 - FinalProglength.length();
    for (int i = 0; i < sub2; i++) {
      FinalProglength = "0" + FinalProglength;
    }


    int counter = 0;
    String tRecord = "";


    
    //Makes text record for each line that is at most 30 bytes in length
    //Stores each text record line in TEXTRECORDS and the length of each line in LENGTHRECORDS
    //also stores the starting address of the first opcode in the line in STARTADDR
    for  (int i = 0; i < FINALOPLIST.size(); i++) {
      if (counter <= 60 && counter + FINALOPLIST.get(i).length() <= 60) {
        if (counter == 0) {
          STARTADDR.add(FINALADDRESS.get(i));
        }
        tRecord += "^" + FINALOPLIST.get(i); 
        counter += FINALOPLIST.get(i).length();
        if (i == FINALOPLIST.size() - 1) {
          TEXTRECORDS.add(tRecord);
          LENGTHRECORDS.add(counter);
        }
      } else {
        TEXTRECORDS.add(tRecord);
        LENGTHRECORDS.add(counter);
        STARTADDR.add(FINALADDRESS.get(i));
        tRecord = "^" + FINALOPLIST.get(i);
        counter = 0 + FINALOPLIST.get(i).length();
        if (i == FINALOPLIST.size() - 1) {
          TEXTRECORDS.add(tRecord);
          LENGTHRECORDS.add(counter);
        }
      }
    }

    System.out.println(TEXTRECORDS);
    System.out.println(LENGTHRECORDS);
    //formats the starting addresses and adds '0's in front if needed
    int sub = 6 - FINALSTARTINGADDRESS.length();
    for (int i = 0; i < sub; i++) {
      FINALSTARTINGADDRESS = "0" + FINALSTARTINGADDRESS;
    }
    //formats the program name to add spaces at the end if less than 6 characters
    sub = 6 - PROGRAMNAME.length();
    for (int i = 0; i < sub; i++) {
      PROGRAMNAME = PROGRAMNAME + " ";
    }

    System.out.println(LENGTHRECORDS);

    //prints header record
    System.out.println("~FINAL OBJECT CODE~");
    System.out.println("H^" + PROGRAMNAME + "^" + FINALSTARTINGADDRESS + "^" + FinalProglength);
    //Print text record
    for (int i = 0; i < LENGTHRECORDS.size(); i++) {
      String hexlen = Integer.toHexString((LENGTHRECORDS.get(i)/2));
      if (hexlen.length() < 2) {
        hexlen = "0" + hexlen;
      }
      String new1 = STARTADDR.get(i);
      int sub1 = 6 - new1.length();
      for (int j = 0; j < sub1; j++) {
        new1 = "0" + new1;
      }
      System.out.println("T^" + new1 + "^" + hexlen.toUpperCase() + TEXTRECORDS.get(i).toUpperCase());
    }

    //Print Modification record
    for (int i = 0; i < MODIFICATIONS.size(); i++) {
      int line = lineFinder(MODIFICATIONS.get(i));
      String addy = addresses.get(line);
      int num = Integer.parseInt(addy, 16);
      num++;
      addy = Integer.toHexString(num);
      int sub3 = 6 - addy.length();
      for (int j = 0; j < sub3; j++) {
        addy = "0" + addy;
      }

      
      System.out.println("M^" + addy + "^05+" +  inputs.get(0));
    }

    //Print End record
    System.out.println("E^" + FINALSTARTINGADDRESS);
    
    //System.out.println(toHex(FINALADDRESS.get(FINALADDRESS.size())));
    
    //System.out.println(FinalProglength);
    
  }





    



  public static boolean isNumeric(final String str) {

        // null or empty
        if (str == null || str.length() == 0) {
            return false;
        }

        for (char c : str.toCharArray()) {
            if (!Character.isDigit(c)) {
                return false;
            }
        }

        return true;

    } 

  public static int lineFinder(int index) {
    int j = 0;
    int max = linelengths.get(j);
    for (int i = 0; i < index; i++) {
      max--;
      if (max == 0) {
        j++;
        max = linelengths.get(j);
      }
    }
    return j;
  }
}